# -*- coding: utf-8 -*-
"""
A setuptools based setup module.
See:
    https://packaging.python.org/en/latest/distributing.html
    https://github.com/pypa/sampleproject
This is required as it is executed while the package is build. 
The script tells conda how to place files inside the package. 
However you may be fine here with some minimal configuration as below.
"""

from setuptools import setup

def main():
    """
    main setup method
    """
    setup(
	# Define the package name, should match the folder structure in your package.
	# If you would have folder/subfolder/script.py your packages var would be:
	# packages=["folder", "folder/subfolder"] 
        packages=["hello"])

if __name__ == '__main__':
    main()

