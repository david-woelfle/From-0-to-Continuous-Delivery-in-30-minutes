# -*- coding: utf-8 -*-
"""
Author: David Wölfle <https://gitlab.com/david-woelfle>

This is a simple example project to showcase coninous integration.
Numpy is solely imported as there should be a dependency to fetch.
"""

import numpy as np


def print_hello():
    print('Hello Pycon!')
    print('We are using NumPy {}'.format(np.__version__))
