This is the repository belonging to my talk "From 0 to Continuous Delivery in 30 minutes" @ PyCon.DE 2017 (https://de.pycon.org/schedule/talks/from-0-to-continuous-delivery-in-30-minutes/).  
  
In this repository you can find the slides as pdf containing some introduction and a step by step instruction set how to set up an example continuous delivery pipeline. The folder "example_project" contains the actual files for the example pipeline. Why each file is relevant and what it does is explained as comments in the files.  
  
If you wish to discuss this project please feel free to connect to and/or contact me via:  
https://www.linkedin.com/in/david-woelfle/  
https://www.xing.com/profile/David_Woelfle  
david[dot]woelfle[dot]sp[at]posteo.de  
